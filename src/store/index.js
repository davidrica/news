import Vue from 'vue';
import Vuex from 'vuex';
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    count: 0,
    newsList: [],
    topNews: null,
    todayNews: [],
    currentNews: null,
    recentlyVisited: [],
    searchedResult: [],
  },
  mutations: {
    increment(state) {
      state.count += 1;
    },
    decrement(state) {
      state.count -= 1;
    },
    setNewsList(state, payload) {
      state.newsList = payload;
    },
    setTopNews(state, payload) {
      state.topNews = payload;
    },
    setTodayNews(state, payload) {
      state.todayNews = payload;
    },
    setCurrentNews(state, payload) {
      state.currentNews = payload;
    },
    addRecentlyVisited(state, payload) {
      state.recentlyVisited.push(payload);
    },
    updateHeadline(state, payload) {
      if (payload.type === "newsList") {
        state.newsList = state.newsList.map((news, index) => (
          index === payload.index ? { ...news, title: payload.text } : news));
      } else {
        state.topNews = { ...state.topNews, title: payload.text };
      }
    },
    searchNews(state, payload) {
      state.searchedResult = payload;
    },
  },
  actions: {
    setNewsList(state) {
      axios
        .get(
          "https://newsapi.org/v2/top-headlines?country=us&apiKey=099148be22804e849a0c6fe022b7cf5e"
          // "http://localhost:8080/mock-data/news-list.json" // mock data
        )
        .then((response) => {
          if (response.status === 200) {
            if (response.data && response.data.articles.length > 0) {
              const { articles } = response.data;
              // const today = new Date().setHours(0, 0, 0, 0);
              let topNews = null;
              const newsList = [];
              const todayNews = [];

              for (let i = 0; i < articles.length; i += 1) {
                // const articleDate = new Date(articles[i].publishedAt);
                if (i === 0) {
                  topNews = articles[i];
                } else if (i < 5) {
                  todayNews.push(articles[i]);
                } else {
                  newsList.push(articles[i]);
                }
              }
              state.commit("setNewsList", newsList);
              state.commit("setTopNews", topNews);
              state.commit("setTodayNews", todayNews);
            }
          }
        });
    },
    setCurrentNews(state, payload) {
      state.commit("setCurrentNews", payload);
      state.commit("addRecentlyVisited", payload);
    },
    searchNews(state, payload) {
      // console.log('search key: ', payload);
      return new Promise((resolve) => {
        axios
          .get(
            `https://newsapi.org/v2/top-headlines?q=${payload}&apiKey=099148be22804e849a0c6fe022b7cf5e`
            // `http://localhost:8080/mock-data/search-news.json` // mock data
          )
          .then((response) => {
            if (response.status === 200) {
              if (response.data && response.data.articles.length > 0) {
                const { articles } = response.data;
                state.commit("searchNews", articles);
                resolve();
              }
            }
          });
      });
    },
  },
  modules: {
  },
  getters: {
    getNewsList: (state) => state.newsList,
    getTopNews: (state) => state.topNews,
    getTodayNews: (state) => state.todayNews,
    getCurrentNews: (state) => state.currentNews,
    getRecentlyVisited: (state) => state.recentlyVisited,
    getSearchedResult: (state) => state.searchedResult,
  }
});
