# news

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

&nbsp;

> Note: This app is not finished yet. I will try to keep develop this app whenever I have spare time.